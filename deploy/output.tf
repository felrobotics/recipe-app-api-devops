output "dbhost" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_lb_endopoint" {
  value = aws_lb.api.dns_name
}

output "api_endopoint" {
  value = aws_route53_record.app.fqdn
}