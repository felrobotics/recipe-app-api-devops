# raad = short for project name (recipe app api devops)

# prefix help us identify our resources, useful if we have many projects
variable "prefix" {
  type    = string
  default = "raad"
}

variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  type    = string
  default = "feposada@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "071184239846.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "071184239846.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret Key por Django App"
}

# TODO: with route53
variable "dns_zone_name" {
  description = "Domain name"
  default     = "posada.dev"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    "production" = "api"
    "staging"    = "api.staging"
    "dev"        = "api.dev"
  }
}